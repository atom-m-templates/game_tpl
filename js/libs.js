 // Запуск FancyBox
$(document).ready(function() {
    $("a.gallery").fancybox();
});

$(document).ready(function() {
  $('.north').tipsy({gravity: 'n'}); 
  $('.south').tipsy({gravity: 's'}); 
  $('.east').tipsy({gravity: 'e'}); 
  $('.west').tipsy({gravity: 'w'});  
}); 

$(document).ready(function() {
  $(".information_edit_user").hide();
  $("#edit_user_group").click(function () {
  $(".information_edit_user").show(300);
  setTimeout(function(){$('.information_edit_user').fadeOut('fast')},3000); 
  });
});

function replyComment(el, id, name){
    $('.comment_tree').removeClass('reply_comment');
    $(el).parents('.comment_tree').addClass('reply_comment');
    $('.addcomment .inftitle').html('Ответ для '+name);
    $('.addcomment input[name=reply]').val(id);
    $('.addcomment .canselReplyComment').css('display', 'inline-block');
}

function canselReplyComment(){
    $('.comment_tree').removeClass('reply_comment');
    $('.addcomment .inftitle').html('');
    $('.addcomment input[name=reply]').val(0);
    $('.addcomment .canselReplyComment').css('display', 'none');
}